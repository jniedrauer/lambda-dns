package main

import (
	"sync"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/ec2"
	"github.com/aws/aws-sdk-go/service/route53"
)

var sessInit sync.Once
var sess *session.Session

/* getSession returns an AWS SDK session singleton */
func getSession() *session.Session {
	sessInit.Do(func() {
		var err error
		sess, err = session.NewSession(&aws.Config{
			Region: aws.String(
				fromEnvOrDefault(envAWSRegion, defaultRegion),
			),
		})

		if err != nil {
			log.Fatalf("Failed to initialize AWS SDK session: %v", err)
		}
	})

	return sess
}

var ec2Init sync.Once
var ec2Client *ec2.EC2

/* getEc2Client returns an EC2 client singleton */
func getEc2Client() *ec2.EC2 {
	ec2Init.Do(func() {
		ec2Client = ec2.New(getSession())
	})

	return ec2Client
}

var route53Init sync.Once
var route53Client *route53.Route53

/* getRoute53Client returns a Route 53 client singleton */
func getRoute53Client() *route53.Route53 {
	route53Init.Do(func() {
		route53Client = route53.New(getSession())
	})

	return route53Client
}
