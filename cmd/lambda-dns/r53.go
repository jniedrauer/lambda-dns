package main

import (
	"fmt"

	"github.com/aws/aws-sdk-go/service/route53"
	"github.com/aws/aws-sdk-go/service/route53/route53iface"
)

type r53Wrapper struct {
	Client route53iface.Route53API
}

/* UpsertRecord creates or updates a Route 53 A record */
func (r53 *r53Wrapper) UpsertRecord(name string, ip string, ttl int64, zoneID string) error {
	action := "UPSERT"
	recordType := defaultRecordType

	log.Debugf("Setting name: %s, ip: %s", name, ip)

	recordSet := route53.ResourceRecordSet{
		Name:            &name,
		TTL:             &ttl,
		Type:            &recordType,
		ResourceRecords: []*route53.ResourceRecord{{Value: &ip}},
	}

	payload := route53.ChangeResourceRecordSetsInput{
		ChangeBatch: &route53.ChangeBatch{
			Changes: []*route53.Change{{
				Action:            &action,
				ResourceRecordSet: &recordSet,
			}},
		},
		HostedZoneId: &zoneID,
	}

	_, err := r53.Client.ChangeResourceRecordSets(&payload)
	if err == nil {
		log.Infof("Updated record: %s with value: %s", name, ip)
	}

	return err
}

/* GetZoneIDForVPC returns the Route 53 private zone ID associated with VPC ID */
func (r53 *r53Wrapper) GetZoneIDForVPC(vpcID string) (zoneID string, err error) {
	input := route53.ListHostedZonesInput{}

	reply, err := r53.Client.ListHostedZones(&input)
	if err != nil {
		return
	}

	for _, zone := range reply.HostedZones {
		if !*zone.Config.PrivateZone {
			continue
		}

		match, err := r53.VPCIDInZone(vpcID, *zone.Id)
		if err != nil {
			log.Warn("Caught an error looking up zone details: %s", err)
			continue
		}

		if match {
			return *zone.Id, nil
		}
	}

	err = fmt.Errorf("Failed to identify hosted zone associated with VPC: %s", vpcID)

	return
}

/* VPCIDInZone checks whether the VPC ID is associated with the zone ID */
func (r53 *r53Wrapper) VPCIDInZone(vpcID string, zoneID string) (result bool, err error) {
	input := route53.GetHostedZoneInput{Id: &zoneID}

	reply, err := r53.Client.GetHostedZone(&input)
	if err != nil {
		return
	}

	for _, vpc := range reply.VPCs {
		if *vpc.VPCId == vpcID {
			return true, nil
		}
	}

	return false, nil
}
