package main

import (
	"encoding/json"
	"fmt"

	"github.com/aws/aws-lambda-go/events"
)

// Deserialize Cloudwatch event and return the instance ID
func getInstanceID(event *events.CloudWatchEvent) (id string, err error) {
	var content stateChange

	err = json.Unmarshal(event.Detail, &content)
	if err != nil {
		err = fmt.Errorf("Failed to deserialize event: %s", err)
		return
	}

	if content.State != validInstanceState {
		err = fmt.Errorf("Invalid instance state: %s", content.State)
		return
	}

	id = content.InstanceID

	log.Debug("Got instance ID:", id)

	return
}
