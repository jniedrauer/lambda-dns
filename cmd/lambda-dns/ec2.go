package main

import (
	"fmt"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/ec2"
	"github.com/aws/aws-sdk-go/service/ec2/ec2iface"
)

type ec2Wrapper struct {
	Client           ec2iface.EC2API
	ID               string
	PublicName       string
	PrivateName      string
	PublicIPAddress  string
	PublicZoneID     string
	PrivateIPAddress string
	VPCID            string
	Blacklisted      bool
}

/* LoadNames loads the instance public and/or private names */
func (c *ec2Wrapper) LoadNames() error {
	payload := ec2.DescribeTagsInput{Filters: []*ec2.Filter{
		{
			Name:   aws.String("resource-id"),
			Values: []*string{&c.ID},
		},
	}}

	response, err := c.Client.DescribeTags(&payload)
	if err != nil {
		return err
	}

	for _, val := range response.Tags {
		if *val.Key == updateTag && *val.Value == "false" {
			log.Debug("Matched blacklist condition")
			c.Blacklisted = true
			return nil
		}

		if *val.Key == publicNameTag {
			c.PublicName = *val.Value
		}

		if *val.Key == publicZoneTag {
			c.PublicZoneID = *val.Value
		}

		if *val.Key == privateNameTag {
			c.PrivateName = *val.Value
		}
	}

	if c.PublicName == "" && c.PrivateName == "" {
		return fmt.Errorf("Failed to find names in tags")
	}

	return nil
}

/* GetIP returns the instance internal IP address */
func (c *ec2Wrapper) LoadNetworkDetails() error {
	payload := ec2.DescribeInstancesInput{Filters: []*ec2.Filter{
		{
			Name:   aws.String("instance-id"),
			Values: []*string{&c.ID},
		},
	}}

	response, err := c.Client.DescribeInstances(&payload)
	if err != nil {
		return err
	}

	if len(response.Reservations) != 1 {
		return fmt.Errorf("Unexpected reservation count returned: %d", len(response.Reservations))
	}

	if len(response.Reservations[0].Instances) != 1 {
		return fmt.Errorf("Unexpected instance count returned: %d", len(response.Reservations[0].Instances))
	}

	if response.Reservations[0].Instances[0].PublicIpAddress != nil {
		c.PublicIPAddress = *response.Reservations[0].Instances[0].PublicIpAddress
	}

	c.PrivateIPAddress, err = getIPFromInterfaces(response.Reservations[0].Instances[0].NetworkInterfaces)
	if err != nil {
		return err
	}

	c.VPCID = *response.Reservations[0].Instances[0].VpcId

	return nil
}

/* getIPFromInterfaces returns the first private IP from a slice of network */
func getIPFromInterfaces(interfaces []*ec2.InstanceNetworkInterface) (ip string, err error) {
	for _, iface := range interfaces {
		if *iface.PrivateIpAddress != "" {
			ip = *iface.PrivateIpAddress
			return
		}
	}

	err = fmt.Errorf("Failed to identify private IP address")
	return
}
