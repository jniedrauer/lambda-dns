package main

import (
	"errors"
	"testing"

	"github.com/aws/aws-sdk-go/service/ec2"
	"github.com/stretchr/testify/assert"
)

type mockClient struct {
	*ec2.EC2
	DescribeTagsResp      *ec2.DescribeTagsOutput
	DescribeInstancesResp *ec2.DescribeInstancesOutput
}

func (c mockClient) DescribeTags(input *ec2.DescribeTagsInput) (*ec2.DescribeTagsOutput, error) {
	return c.DescribeTagsResp, nil
}

func (c mockClient) DescribeInstances(input *ec2.DescribeInstancesInput) (*ec2.DescribeInstancesOutput, error) {
	return c.DescribeInstancesResp, nil
}

func TestLoadNames(t *testing.T) {
	tests := []struct {
		inputPrivateKey string
		inputPublicKey  string
		expectPrivate   string
		expectPublic    string
		err             error
	}{
		// Assert that the private name tag is returned
		{
			inputPrivateKey: privateNameTag,
			inputPublicKey:  publicNameTag,
			expectPrivate:   "TestPrivateName",
			expectPublic:    "TestPublicName",
			err:             nil,
		},
		// Assert that an error is raised if no name tag is found
	}

	for _, test := range tests {
		junkKey := "JunkKey"
		junkValue := "JunkValue"

		tags := []*ec2.TagDescription{
			{Key: &test.inputPrivateKey, Value: &test.expectPrivate},
			{Key: &test.inputPublicKey, Value: &test.expectPublic},
			{Key: &junkKey, Value: &junkValue},
		}
		i := ec2Wrapper{Client: mockClient{
			DescribeTagsResp: &ec2.DescribeTagsOutput{Tags: tags},
		}}

		err := i.LoadNames()

		assert.IsType(t, test.err, err)
		assert.Equal(t, test.expectPrivate, i.PrivateName)
		assert.Equal(t, test.expectPublic, i.PublicName)
	}
}

func TestLoadNetworkDetails(t *testing.T) {
	tests := []struct {
		reservations  int
		instances     int
		expectPrivate string
		expectPublic  string
		vpcID         string
		err           error
	}{
		// Single private IP address
		{
			reservations:  1,
			instances:     1,
			expectPrivate: "123.123.123.123",
			vpcID:         "vpc-123",
			err:           nil,
		},
		// Public and private IPs
		{
			reservations:  1,
			instances:     1,
			expectPublic:  "123.123.123.123",
			expectPrivate: "123.123.123.124",
			vpcID:         "vpc-123",
			err:           nil,
		},
		// No reservations returned
		{
			reservations: 0,
			err:          errors.New(""),
		},
		// No instances returned
		{
			reservations: 1,
			instances:    0,
			err:          errors.New(""),
		},
		// No IP address returned
		{
			reservations: 1,
			instances:    1,
			err:          errors.New(""),
		},
	}

	for _, test := range tests {
		reply := &ec2.DescribeInstancesOutput{}

		for i := 0; i < test.reservations; i++ {
			reply.Reservations = append(reply.Reservations, &ec2.Reservation{})
		}

		for i := 0; i < test.instances; i++ {
			reply.Reservations[0].Instances = append(
				reply.Reservations[0].Instances, &ec2.Instance{VpcId: &test.vpcID},
			)
		}

		if test.expectPrivate != "" {
			privateInterface := []*ec2.InstanceNetworkInterface{{PrivateIpAddress: &test.expectPrivate}}
			reply.Reservations[0].Instances[0].NetworkInterfaces = privateInterface
		}

		if test.expectPublic != "" {
			reply.Reservations[0].Instances[0].PublicIpAddress = &test.expectPublic
		}

		i := ec2Wrapper{Client: mockClient{DescribeInstancesResp: reply}}

		err := i.LoadNetworkDetails()

		assert.IsType(t, test.err, err)
		assert.Equal(t, test.expectPrivate, i.PrivateIPAddress)
		assert.Equal(t, test.expectPublic, i.PublicIPAddress)
		assert.Equal(t, test.vpcID, i.VPCID)
	}
}
