package main

import (
	"errors"
	"os"
	"testing"

	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
)

func TestGetLogLevel(t *testing.T) {
	tests := []struct {
		lvl    string
		expect logrus.Level
		err    error
	}{
		{
			lvl:    "DEBUG",
			expect: logrus.DebugLevel,
			err:    nil,
		},
		{
			lvl:    "INFO",
			expect: logrus.InfoLevel,
			err:    nil,
		},
		// Fallback to default log level
		{
			lvl:    "",
			expect: defaultLogLevel,
			err:    nil,
		},
		// Raise error on fake log level
		{
			lvl: "notreal",
			err: errors.New(""),
		},
	}

	for _, test := range tests {
		if test.lvl == "" {
			os.Unsetenv(envLogLevel)
		} else {
			os.Setenv(envLogLevel, test.lvl)
		}

		result, err := getLogLevel()

		assert.IsType(t, test.err, err)
		assert.Equal(t, test.expect, result)
	}
}

func TestFromEnvOrDefault(t *testing.T) {
	tests := []struct {
		val    string
		def    string
		expect string
	}{
		{
			val:    "val1",
			def:    "val2",
			expect: "val1",
		},
		{
			val:    "",
			def:    "val2",
			expect: "val2",
		},
	}

	testEnvVar := "TEST_ENV_VAR"

	for _, test := range tests {
		if test.val == "" {
			os.Unsetenv(testEnvVar)
		} else {
			os.Setenv(testEnvVar, test.val)
		}

		result := fromEnvOrDefault(testEnvVar, test.def)

		assert.Equal(t, test.expect, result)
	}
}
