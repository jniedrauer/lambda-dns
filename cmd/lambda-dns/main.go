/*
An AWS Lambda function for controlling DNS at the VPC level
using Route 53
*/
package main

import (
	"fmt"
	"strconv"
	"time"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/sirupsen/logrus"
)

var log = logrus.New()

const envAWSRegion = "AWS_REGION"
const defaultRegion = "us-east-1"

const envLogLevel = "LOG_LEVEL"
const defaultLogLevel = logrus.InfoLevel

const validInstanceState = "running"
const privateNameTag = "PrivateName"
const publicNameTag = "PublicName"
const publicZoneTag = "PublicZoneId"
const updateTag = "UpdateDns"

const envPrivateTTL = "PRIVATE_TTL"
const envPublicTTL = "PUBLIC_TTL"
const defaultTTL = "300"
const defaultRecordType = "A"

var publicTTL int64
var privateTTL int64
var privateZoneID string

type response struct {
	Message string `json:"message"`
	Ok      bool   `json:"ok"`
}

type stateChange struct {
	InstanceID string `json:"instance-id"`
	State      string `json:"state"`
}

func init() {
	logLevel, err := getLogLevel()

	log.SetLevel(logLevel)

	if err != nil {
		log.Error(err)
	}

	publicTTL, err = strconv.ParseInt(fromEnvOrDefault(envPublicTTL, defaultTTL), 10, 64)
	if err != nil {
		panic(err)
	}

	privateTTL, err = strconv.ParseInt(fromEnvOrDefault(envPrivateTTL, defaultTTL), 10, 64)
	if err != nil {
		panic(err)
	}
}

func main() {
	lambda.Start(lambdaHandler)
}

// Lambda entrypoint
func lambdaHandler(event *events.CloudWatchEvent) (msg response, err error) {
	log.Debug("Got event: ", event)

	id, err := getInstanceID(event)
	if err != nil {
		return
	}

	instance := ec2Wrapper{
		Client: getEc2Client(),
		ID:     id,
	}

	for i := 0; i < 5; i++ {
		err = instance.LoadNames()
		if instance.PrivateName != "" || instance.PublicName != "" {
			break
		}
		time.Sleep(5 * time.Second)
	}
	if err != nil {
		return
	}

	if instance.Blacklisted == true {
		msg = response{
			Message: fmt.Sprintf("Instance tagged with: %s = false, exiting", updateTag),
			Ok:      true,
		}

		return
	}

	err = instance.LoadNetworkDetails()
	if err != nil {
		return
	}

	r53 := r53Wrapper{
		Client: getRoute53Client(),
	}

	if instance.PublicName != "" {
		err = r53.UpsertRecord(instance.PublicName, instance.PublicIPAddress, publicTTL, instance.PublicZoneID)
		if err != nil {
			return
		}
	}

	if instance.PrivateName != "" {
		privateZoneID, err = r53.GetZoneIDForVPC(instance.VPCID)
		if err != nil {
			return
		}

		err = r53.UpsertRecord(instance.PrivateName, instance.PrivateIPAddress, privateTTL, privateZoneID)
		if err != nil {
			return
		}
	}

	msg = response{
		Message: fmt.Sprintf(
			"PublicName: %s, PublicIP: %s, PrivateName: %s, PrivateIP: %s",
			instance.PublicName, instance.PublicIPAddress,
			instance.PrivateName, instance.PrivateIPAddress,
		),
		Ok: true,
	}

	return
}
