package main

import (
	"os"

	"github.com/sirupsen/logrus"
)

// Get log level from an environment variable
func getLogLevel() (level logrus.Level, err error) {
	val, set := os.LookupEnv(envLogLevel)
	if set {
		level, err = logrus.ParseLevel(val)
	} else {
		level = defaultLogLevel
	}

	return
}

/* Given an environment variable and a default value, return
environment variable's value or default if unset */
func fromEnvOrDefault(env string, otherwise string) string {
	val, set := os.LookupEnv(env)
	if set {
		return val
	}
	return otherwise
}
