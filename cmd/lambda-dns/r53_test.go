package main

import (
	"testing"

	"github.com/aws/aws-sdk-go/service/route53"
	"github.com/stretchr/testify/assert"
)

type mockR53Client struct {
	*route53.Route53
	GetHostedZoneResp *route53.GetHostedZoneOutput
}

func (c *mockR53Client) GetHostedZone(input *route53.GetHostedZoneInput) (*route53.GetHostedZoneOutput, error) {
	return c.GetHostedZoneResp, nil
}

func TestVPCIDInZone(t *testing.T) {
	testVPCID := "vpc-123"
	testUnmatchingVPCID := "vpc-234"

	tests := []struct {
		vpcID  string
		expect bool
		err    error
		vpcs   []*route53.VPC
	}{
		// Single matching VPC
		{
			vpcID:  testVPCID,
			expect: true,
			err:    nil,
			vpcs: []*route53.VPC{
				{VPCId: &testVPCID},
			},
		},
		// No match found
		{
			vpcID:  "NotAMatch",
			expect: false,
			err:    nil,
			vpcs: []*route53.VPC{
				{VPCId: &testVPCID},
			},
		},
		// Zone with multiple VPCs
		{
			vpcID:  testVPCID,
			expect: true,
			err:    nil,
			vpcs: []*route53.VPC{
				{VPCId: &testUnmatchingVPCID},
				{VPCId: &testVPCID},
			},
		},
	}

	for _, test := range tests {
		c := mockR53Client{GetHostedZoneResp: &route53.GetHostedZoneOutput{
			VPCs: test.vpcs,
		}}

		r := r53Wrapper{Client: &c}

		result, err := r.VPCIDInZone(test.vpcID, "")

		assert.IsType(t, test.err, err)
		assert.Equal(t, test.expect, result)
	}
}
