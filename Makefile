# vim: set noet

# Go parameters
GO = go
DEP = dep

BUILDDIR := $(shell pwd)/build

all: test build

build: dep
	GOBIN=$(BUILDDIR) $(GO) install -v \
		  -race -ldflags "-extldflags '-static'" \
		  $(shell go list ./... | grep -v /vendor/)
	cd $(BUILDDIR) && zip lambda-dns.zip lambda-dns

test: dep
	$(GO) test -v \
		-race \
		$(shell go list ./... | grep -v /vendor/)

clean:
	$(GO) clean -cache -testcache

dep:
	$(DEP) ensure -v

.PHONY: build test clean dep
