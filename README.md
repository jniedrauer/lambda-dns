# lambda-dns
Control DNS at the VPC level using Route 53 and AWS Lambda

[![Go Report Card](https://goreportcard.com/badge/gitlab.com/jniedrauer/lambda-dns)](https://goreportcard.com/report/gitlab.com/jniedrauer/lambda-dns)
[![Build Status](https://gitlab.com/jniedrauer/lambda-dns/badges/master/pipeline.svg)](https://gitlab.com/jniedrauer/lambda-dns/commits/master)

## Building
Building requires the [dep](https://github.com/golang/dep) dependency manager
for Go. You can install it via `go get`:

```
go get -u github.com/golang/dep/cmd/dep
```

Default make target will download dependencies, run unit tests, and compile.

## Use
The binary artifacts are designed for use in AWS Lambda.

### What it does
The Lambda function will read metadata from the EC2 instance,
determine the Route 53 private zone associated with the VPC,
and send an UPSERT request of the following form:

- Type: A

- Name: EC2 instance `PrivateName` or `PublicName` tags

  An error will be raised if neither of these tags are set

- Values: First matched private or public IP address associated with the
  EC2 instance, respectively

### Function trigger
Set the Lambda function to trigger on EC2 instance state change.
The Cloudwatch trigger should look like this:

```
{
  "detail-type": [
    "EC2 Instance State-change Notification"
  ],
  "source": [
    "aws.ec2"
  ],
  "detail": {
    "state": [
      "running"
    ]
  }
}
```

### Minimum IAM permissions
You should give the function a policy similar to this:

```
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "ec2:DescribeInstances",
				"ec2:DescribeTags"
            ],
            "Resource": "*"
        },
        {
            "Effect": "Allow",
            "Action": [
                "route53:ListHostedZones",
                "route53:GetHostedZone"
            ],
            "Resource": "*"
        },
        {
            "Effect": "Allow",
            "Action": [
                "route53:ChangeResourceRecordSets"
            ],
            "Resource": [
				"arn:aws:route53:::hostedzone/Your internal hosted zone ID here"
			]
        }
    ]
}
```

### Environment variables
- `LOG_LEVEL`

   Set the log level of the Lambda function.
